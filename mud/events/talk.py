from .event import Event1, Event2, Event3

class TalkEvent(Event2):
	NAME = "talk"
	def perform(self):
		if self.object not in self.actor or self.object not in self.container:
			self.buffer_clear()
			self.buffer_inform("talk.actor")
			self.actor.send_result(self.buffer_get())


