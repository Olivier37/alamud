from .action import Action, Action1, Action2, Action3
from mud.events import TalkEvent

class TalkAction(Action2):
	EVENT = TalkEvent
	RESOLVE_OBJECT = "resolve_for_take"
	ACTION = "talk"
